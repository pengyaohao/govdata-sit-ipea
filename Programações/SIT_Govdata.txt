CREATE TABLE treinamento.caged_e AS 
SELECT num_cnpj, num_cnae_2_0_classe, num_cnae_2_0_divisao, num_cnae_2_0_grupo, tip_cnae_2_0_secao, num_municipio_estabelecimento, tip_estabelecimento,
qtd_mov_primeiro_emprego, qtd_mov_aposentadoria, qtd_mov_com_justa_causa, qtd_mov_pedido_desligamento, qtd_mov_falecimento, qtd_mov_prazo_determinado, qtd_mov_reemprego,
qtd_mov_reintegracao, qtd_mov_sem_justa_causa, qtd_mov_termino_contrato, qtd_mov_termino_contrato_tempo_determinado, qtd_mov_transferencia_entrada, qtd_mov_transferencia_saida,
num_saldo_movimentacao, qtd_empregados_inicio_janeiro, qtd_empregados_primeiro_dia_mes, qtd_empregados_ultimo_dia_mes
FROM bgcaged.caged_estabelecimento;

CREATE TABLE mte.govdata_teste AS 
SELECT num_pis_trabalhador, num_cpf_trabalhador, vlr_salario_mensal_contratual, vlr_salario_mensal_declarado, num_cnpj, num_cnae_2_0_classe, num_cnae_2_0_divisao, num_cnae_2_0_grupo,tip_cnae_2_0_secao, num_municipio_estabelecimento, tip_estabelecimento, tip_movimento_desagregado, dia_desligamento, 
idt_aprendiz, tip_faixa_hora_contratual, tip_faixa_salario_mensal, tip_faixa_tempo_emprego, num_cbo, num_cbo_ocupacao, tip_grau_instrucao, idt_portador_deficiencia, tip_deficiencia, tip_raca_cor, 
tip_sexo, num_idade, qtd_salarios_minimos, num_tempo_emprego, qtd_horas_contratuais, ams_competencia
FROM bgcaged.caged_trabalhador
WHERE ams_competencia = "201601" OR ams_competencia = "201602" OR ams_competencia = "201603" OR ams_competencia = "201604" OR ams_competencia = "201605" OR ams_competencia = "201606" OR ams_competencia = "201607" OR ams_competencia = "201608" OR ams_competencia = "201609" OR ams_competencia = "201610" OR ams_competencia = "201611" OR ams_competencia = "201612";

DROP TABLE mte.atest

CREATE TABLE treinamento.rais_e AS 
SELECT num_cnpj, num_cnae_2_0_classe, num_cnae_2_0_divisao, num_cnae_2_0_grupo, num_municipio, tip_estabelecimento, idt_doc_ident, num_porte_estabelecimento, qtd_tamanho_estabelecimento, qtd_vinculos_ativos_pdd_clt
FROM bgrais.rais_estabelecimento;

CREATE TABLE treinamento.rais_t AS 
SELECT num_pasep_pis_nit_nis, num_cpf, num_ctps, vlr_salario_contratual, num_cnpj, num_cnae_2_0_classe, num_cnae_2_0_divisao, num_cnae_2_0_grupo,tip_cnae_2_0_secao, num_municipio, tip_estabelecimento, idt_doc_ident, qtd_tamanho_estabelecimento, tip_causa_afastamento_1, tip_causa_afastamento_2, tip_causa_afastamento_3, num_cbo_2002, 
num_cbo_2002_ocupacao_especifica, tip_faixa_hora_contratual, tip_faixa_media_hora_extra,  tip_faixa_remuneracao_media, qtd_dias_afastamento, qtd_meses_hora_extra, qtd_meses_reajuste_coletivo, qtd_tempo_emprego, tip_admissao, tip_vinculo, tip_salario, vlr_gratificacao, vlr_hora_extra, vlr_multa_rescisoria,vlr_remuneracao_janeiro_com_corte, vlr_remuneracao_fevereiro_com_corte, vlr_remuneracao_marco_com_corte, vlr_remuneracao_abril_com_corte, vlr_remuneracao_maio_com_corte,
vlr_remuneracao_junho_com_corte, vlr_remuneracao_julho_com_corte, vlr_remuneracao_agosto_com_corte, vlr_remuneracao_setembro_com_corte, vlr_remuneracao_outubro_com_corte, vlr_remuneracao_novembro_com_corte, vlr_remuneracao_dezembro_com_corte,vlr_ultima_remuneracao_ano, tip_grau_instrucao, idt_portador_deficiencia, tip_deficiencia, tip_raca, tip_sexo, tip_nacionalidade, num_idade, qtd_hora_contratual, qtd_hora_extra_janeiro, qtd_hora_extra_fevereiro, qtd_hora_extra_marco, qtd_hora_extra_abril, qtd_hora_extra_maio, qtd_hora_extra_junho,qtd_hora_extra_julho 
qtd_hora_extra_agosto, qtd_hora_extra_setembro, qtd_hora_extra_outubro, qtd_hora_extra_novembro, qtd_hora_extra_dezembro,qtd_tempo_emprego_ano_base, vlr_adiantamento_13_salario, vlr_aviso_previo, vlr_complemento_13_salario, vlr_reajuste_coletivo, vlr_ferias_vencidas_proporcionais
FROM bgrais.rais_trabalhador;


CREATE TABLE treinamento.caged_rais AS 
SELECT S.num_cnpj, s.num_cnae_2_0_classe, s.num_cnae_2_0_divisao, s.num_cnae_2_0_grupo, s.tip_cnae_2_0_secao, s.num_municipio_estabelecimento, s.tip_estabelecimento,
s.qtd_mov_primeiro_emprego, s.qtd_mov_aposentadoria, s.qtd_mov_com_justa_causa, s.qtd_mov_pedido_desligamento, s.qtd_mov_falecimento, s.qtd_mov_prazo_determinado, s.qtd_mov_reemprego,
s.qtd_mov_reintegracao, s.qtd_mov_sem_justa_causa, s.qtd_mov_termino_contrato, s.qtd_mov_termino_contrato_tempo_determinado, s.qtd_mov_transferencia_entrada, s.qtd_mov_transferencia_saida,
s.num_saldo_movimentacao, s.qtd_empregados_inicio_janeiro, s.qtd_empregados_primeiro_dia_mes, s.qtd_empregados_ultimo_dia_mes
FROM bgcaged.caged_estabelecimento AS S
    JOIN bgcaged.caged_trabalhador AS T
    ON S.num_cnpj = T.num_cnpj
    JOIN bgrais.rais_estabelecimento AS RE
    ON T.num_cnpj = RE.num_cnpj
    JOIN bgrais.rais_trabalhador AS RT
    ON RE.num_cnpj = RT.num_cnpj
    ;


CREATE TABLE treinamento.teste2 AS
SELECT S.num_cnpj, s.num_cnae_2_0_classe, s.num_cnae_2_0_divisao, s.num_cnae_2_0_grupo, s.tip_cnae_2_0_secao, s.num_municipio_estabelecimento, s.tip_estabelecimento,
s.qtd_mov_primeiro_emprego, s.qtd_mov_aposentadoria, s.qtd_mov_com_justa_causa, s.qtd_mov_pedido_desligamento, s.qtd_mov_falecimento, s.qtd_mov_prazo_determinado, s.qtd_mov_reemprego,
s.qtd_mov_reintegracao, s.qtd_mov_sem_justa_causa, s.qtd_mov_termino_contrato, s.qtd_mov_termino_contrato_tempo_determinado, s.qtd_mov_transferencia_entrada, s.qtd_mov_transferencia_saida,
s.num_saldo_movimentacao, s.qtd_empregados_inicio_janeiro, s.qtd_empregados_primeiro_dia_mes, s.qtd_empregados_ultimo_dia_mes
FROM treinamento.caged_e AS S
FULL JOIN treinamento.caged_trab AS T 
ON S.num_cnpj = T.num_cnpj 
FULL JOIN treinamento.rais_e AS RE 
ON T.num_cnpj = RE.num_cnpj
FULL JOIN treinamento.rais_t AS RT
ON RE.num_cnpj = RT.num_cnpj;

drop table treinamento.teste2;
DROP TABLE treinamento.caged_t;

SELECT DISTINCT ano_base_rais
FROM bgrais.rais_trabalhador;

SELECT DISTINCT ano_base_rais
FROM bgrais.rais_estabelecimento;

SELECT DISTINCT ano_base
FROM rais_operacional.estabelecimento;

SELECT DISTINCT ano_base
FROM rais_operacional.vinculo;

SELECT DISTINCT ams_competencia
FROM bgcaged.caged_estabelecimento;

SELECT DISTINCT ams_competencia
FROM bgcaged.caged_trabalhador;

SELECT DISTINCT ano_base
FROM rais_operacional.estabelecimento;

SELECT DISTINCT ano_base
FROM rais_operacional.vinculo;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 15/01/2019
CREATE TABLE treinamento.k_caged_e as
SELECT * FROM bgcaged.caged_estabelecimento LIMIT 10000;

CREATE TABLE treinamento.k_caged_t as
SELECT * FROM bgcaged.caged_trabalhador LIMIT 10000;

CREATE TABLE treinamento.k_rais_e as
SELECT * FROM rais_operacional.estabelecimento LIMIT 10000;

CREATE TABLE treinamento.k_rais_t as
SELECT * FROM rais_operacional.vinculo LIMIT 10000;




CREATE TABLE treinamento.k_caged_rais_2017 as
SELECT 
S.num_cnpj, s.num_cnae_2_0_classe, s.num_cnae_2_0_divisao, s.num_cnae_2_0_grupo, s.tip_cnae_2_0_secao, s.num_municipio_estabelecimento, s.tip_estabelecimento,
s.qtd_mov_primeiro_emprego, s.qtd_mov_aposentadoria, s.qtd_mov_com_justa_causa, s.qtd_mov_pedido_desligamento, s.qtd_mov_falecimento, s.qtd_mov_prazo_determinado, s.qtd_mov_reemprego,s.qtd_mov_reintegracao, s.qtd_mov_sem_justa_causa, s.qtd_mov_termino_contrato, s.qtd_mov_termino_contrato_tempo_determinado, s.qtd_mov_transferencia_entrada, s.qtd_mov_transferencia_saida,s.num_saldo_movimentacao, s.qtd_empregados_inicio_janeiro, s.qtd_empregados_primeiro_dia_mes, s.qtd_empregados_ultimo_dia_mes,
s.ams_competencia
FROM treinamento.k_caged_e AS S
JOIN treinamento.k_caged_t AS T ON S.num_cnpj = T.num_cnpj and s.ams_competencia=t.ams_competencia
JOIN treinamento.k_rais_e AS RE ON T.num_cnpj = RE.num_cnpj_cei and substr(t.ams_competencia,1,4)=re.ano_base
JOIN treinamento.k_rais_t AS RT ON RE.num_cnpj_cei = RT.num_cnpj_cei and re.ano_base=rt.ano_base and rt.num_cpf=t.num_cpf_trabalhador  
WHERE substr(s.ams_competencia,1,4)=2017

DROP TABLE treinamento.k_caged_rais_2017;

DROP TABLE treinamento.k_rais_t;


CREATE TABLE treinamento.k_caged_rais_2017 as
SELECT 
S.num_cnpj, s.num_cnae_2_0_classe, s.num_cnae_2_0_divisao, s.num_cnae_2_0_grupo, s.tip_cnae_2_0_secao, s.num_municipio_estabelecimento, s.tip_estabelecimento,
s.qtd_mov_primeiro_emprego, s.qtd_mov_aposentadoria, s.qtd_mov_com_justa_causa, s.qtd_mov_pedido_desligamento, s.qtd_mov_falecimento, s.qtd_mov_prazo_determinado, s.qtd_mov_reemprego,s.qtd_mov_reintegracao, s.qtd_mov_sem_justa_causa, s.qtd_mov_termino_contrato, s.qtd_mov_termino_contrato_tempo_determinado, s.qtd_mov_transferencia_entrada, s.qtd_mov_transferencia_saida,s.num_saldo_movimentacao, s.qtd_empregados_inicio_janeiro, s.qtd_empregados_primeiro_dia_mes, s.qtd_empregados_ultimo_dia_mes,
s.ams_competencia
FROM bgcaged.caged_estabelecimento AS S
JOIN bgcaged.caged_trabalhador AS T ON S.num_cnpj = T.num_cnpj and s.ams_competencia=t.ams_competencia
JOIN rais_operacional.estabelecimento AS RE ON T.num_cnpj = RE.num_cnpj_cei and substr(t.ams_competencia,1,4)=re.ano_base
JOIN rais_operacional.vinculo AS RT ON RE.num_cnpj_cei = RT.num_cnpj_cei and re.ano_base=rt.ano_base and rt.num_cpf=t.num_cpf_trabalhador  
WHERE substr(s.ams_competencia,1,4)=2017;


CREATE TABLE treinamento.k_caged_2017 as
SELECT S.num_cnpj, s.num_cnae_2_0_classe, s.num_cnae_2_0_divisao, s.num_cnae_2_0_grupo, s.tip_cnae_2_0_secao, s.num_municipio_estabelecimento, s.tip_estabelecimento,
s.qtd_mov_primeiro_emprego, s.qtd_mov_aposentadoria, s.qtd_mov_com_justa_causa, s.qtd_mov_pedido_desligamento, s.qtd_mov_falecimento,
s.qtd_mov_prazo_determinado, s.qtd_mov_reemprego,s.qtd_mov_reintegracao, s.qtd_mov_sem_justa_causa, s.qtd_mov_termino_contrato,
s.qtd_mov_termino_contrato_tempo_determinado, s.qtd_mov_transferencia_entrada, s.qtd_mov_transferencia_saida,s.num_saldo_movimentacao,
s.qtd_empregados_inicio_janeiro, s.qtd_empregados_primeiro_dia_mes, s.qtd_empregados_ultimo_dia_mes,s.ams_competencia,
T.num_pis_trabalhador, T.num_cpf_trabalhador, T.vlr_salario_mensal_contratual, T.vlr_salario_mensal_declarado, T.tip_movimento_desagregado,
T.idt_aprendiz, T.tip_faixa_hora_contratual, T.tip_faixa_salario_mensal, T.num_cbo, T.num_cbo_ocupacao, T.tip_grau_instrucao,
T.idt_portador_deficiencia, T.tip_deficiencia, T.tip_raca_cor, T.tip_sexo, T.num_idade, T.qtd_salarios_minimos, T.num_tempo_emprego, T.qtd_horas_contratuais
FROM bgcaged.caged_estabelecimento AS S
INNER JOIN bgcaged.caged_trabalhador AS T ON S.num_cnpj = T.num_cnpj and s.ams_competencia=t.ams_competencia
WHERE substr(s.ams_competencia,1,4)=2017;

SELECT DISTINCT ams_competencia
FROM treinamento.k_caged_2017;

SELECT DISTINCT ano_base
FROM rais_operacional.estabelecimento;

SELECT DISTINCT ano_base
FROM rais_operacional.vinculo;

CREATE TABLE treinamento.k_rais_2017 as
SELECT RE.num_cnpj_cei, RE.nom_razao_social, RE.cod_municipio_ibge, RE.cod_cnae20, RE.cod_cnae_srf, RE.tip_porte_empresa, RE.qtd_vinculo,
RV.num_pis, RV.nom_participante, RV.tip_sexo, RV.dat_nascimento, RV.num_ctps, RV.num_serie_ctps, RV.num_cpf, RV.cod_grau_instrucao,
RV.tip_vinculo, RV.cod_cbo, RV.cod_causa_desligamento, RV.qtd_hora_semanal,RV.qtd_mes_hora_extra, RV.vlr_remuneracao_jan,
RE.vlr_remuneracao_fev, RV.vlr_remuneracao_mar, RE.vlr_remuneracao_abr, RV.vlr_remuneracao_mai,RE.vlr_remuneracao_jun, RE.vlr_remuneracao_jul,
RE.vlr_remuneracao_ago, RV.vlr_remuneracao_set, RV.vlr_remuneracao_out, RV.vlr_remuneracao_nov,RE.vlr_remuneracao_dez, RE.vlr_remuneracao_13s,
RV.qtd_dia_afastamento, RV.vlr_dissidio_coletivo, RV.qtd_mes_dissidio_coletivo, RV.vlr_salario_contrato,RV.qtd_salario_minimo_ano, 
RV.qtd_media_salario_minimo_ano, RV.vlr_13_salario_final, RV.vlr_13_salario_adiantado, RV.cod_nacionalidade
FROM rais_operacional.estabelecimento as RE
INNER JOIN rais_operacional.vinculo as RV ON RE.num_cnpj_cei = RV.num_cnpj_cei
WHERE RE.tip_inscricao=1 and RV.tip_inscricao=1 and RE.ano_base=2017 and RV.ano_base=2017;

CREATE TABLE mte.k_caged_rais_2017 as
SELECT A.num_cnpj, A.num_cnae_2_0_classe, A.num_cnae_2_0_divisao, A.num_cnae_2_0_grupo, A.tip_cnae_2_0_secao, A.num_municipio_estabelecimento, A.tip_estabelecimento,
A.qtd_mov_primeiro_emprego, A.qtd_mov_aposentadoria, A.qtd_mov_com_justa_causa, A.qtd_mov_pedido_desligamento, A.qtd_mov_falecimento,
A.qtd_mov_prazo_determinado, A.qtd_mov_reemprego,A.qtd_mov_reintegracao, A.qtd_mov_sem_justa_causa, A.qtd_mov_termino_contrato,
A.qtd_mov_termino_contrato_tempo_determinado, A.qtd_mov_transferencia_entrada, A.qtd_mov_transferencia_saida,A.num_saldo_movimentacao,
A.qtd_empregados_inicio_janeiro, A.qtd_empregados_primeiro_dia_mes, A.qtd_empregados_ultimo_dia_mes,A.ams_competencia,
A.num_pis_trabalhador, A.num_cpf_trabalhador, A.vlr_salario_mensal_contratual, A.vlr_salario_mensal_declarado, A.tip_movimento_desagregado,
A.idt_aprendiz, A.tip_faixa_hora_contratual, A.tip_faixa_salario_mensal, A.num_cbo, A.num_cbo_ocupacao, A.tip_grau_instrucao,
A.idt_portador_deficiencia, A.tip_deficiencia, A.tip_raca_cor, A.tip_sexo, A.num_idade, A.qtd_salarios_minimos, A.num_tempo_emprego, A.qtd_horas_contratuais,
B.num_cnpj_cei, B.nom_razao_social, B.cod_municipio_ibge, B.cod_cnae20, B.cod_cnae_srf, B.tip_porte_empresa, B.qtd_vinculo,
B.num_pis, B.nom_participante, B.dat_nascimento, B.num_ctps, B.num_serie_ctps, B.num_cpf, B.cod_grau_instrucao,
B.tip_vinculo, B.cod_cbo, B.cod_causa_desligamento, B.qtd_hora_semanal,B.qtd_mes_hora_extra, B.vlr_remuneracao_jan,
B.vlr_remuneracao_fev, B.vlr_remuneracao_mar, B.vlr_remuneracao_abr, B.vlr_remuneracao_mai,B.vlr_remuneracao_jun, B.vlr_remuneracao_jul,
B.vlr_remuneracao_ago, B.vlr_remuneracao_set, B.vlr_remuneracao_out, B.vlr_remuneracao_nov,B.vlr_remuneracao_dez, B.vlr_remuneracao_13s,
B.qtd_dia_afastamento, B.vlr_dissidio_coletivo, B.qtd_mes_dissidio_coletivo, B.vlr_salario_contrato,B.qtd_salario_minimo_ano, 
B.qtd_media_salario_minimo_ano, B.vlr_13_salario_final, B.vlr_13_salario_adiantado, B.cod_nacionalidade
FROM treinamento.k_caged_2017 as A
INNER JOIN treinamento.k_rais_2017 as B ON A.num_cnpj = B.num_cnpj_cei;
